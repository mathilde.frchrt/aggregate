# Aggregate
(...contributions)

# Introduction
Aggregate is an R package for the aggregation of methods from raw data.
The package extracts the data of interest from an SQL database containing a 
measurement table (according to the OMOP model measurement table model) and a 
period table defined by an SQL script (case studies are described in the 
vignette with an example script).

[article]

This package offers several integrated methods such as : 
- calculation of the maximum value without artefacts,
- calculation of the minimum value without artefacts,
- calculation of the average between two defined values (maximum and minimum). 

More advanced algorithms are also integrated in the package and are used for 
retrospective studies. One of them allows the automatic detection and 
calculation of time periods outside a predefined threshold. Other functions 
derive from this algorithm to, for example, calculate the number of episodes 
detected over a specific period of time.
In more detail, in some areas, when a person stays for a long time with abnormal 
measurements, complications can occur. For example :

- **Anaesthesia**: during manaesthesia the vital signs are monitored. 
A patient who has been hypotensive for more than 20 minutes in total during 
anaesthesia must have specific post-operative care. 
- **Psychiatry**: psychiatric patients are assessed by scores calculated on 
scales. A patient who has spent several days beyond a score threshold can be 
treated according to the severity of his condition.

![](extras/measurementsGraph.png)

The results of these functions are stored and implemented in the SQL database 
under an "Aggregate" table.
[Aggregate table](https://gitlab.com/mathilde.frchrt/aggregate/-/blob/master/inst/sql/sql_server/ddl_aggregate.sql)

# Features 
- Extracts the necessary data from a database in OMOP Common Data Model format 
  (measurement and period tables).
- Calculates aggregation methods from a list of parameters as input to the 
  package.
- The period of interest is chosen by parameters to be defined. A specific 
   period id can be specified, the  subjects ids and measurement 
   concepts ids.
- Detect each episode outside a predefined threshold for longitudinal data 
(e.g. heart rate, psychiatric scale).
- Aggregate the different episodes of a patient to have a total duration. 
- Aggregate maximum, maximum values without artefacts.
- Aggregate average across two values.
- Area between measurement curve and threshold line.
- Missing values is managed to ensure calculation on each patient. 

# Case studies
Different case studies are available and illustrated in the vignette. 
SQL scripts are available to create the period tables for each of the case studies.

1. Case study 1: Detection of acute renal failure during the hospital stay.
2. Case study 2: Hypotension during the first post-operative ICU stay.
3. Case study 3: Psychiatric scale after a visit (or new treatment).
4. Case study 4: Duration of hypotension during general anesthesia.
5. Case study 5: Measures between the taking of a medicinal product and a procedure.

# Results
![](extras/resultHypotension.png)

# Installation 

1. Installing RStudio 
2. In R this command installs the aggregate package:

```
install.packages("drat")
drat::addRepo("mathilde.frchrt/aggregate")
install.packages("aggregate")
```

To have access to the documentation and details of the different functions of the 
package just run the command:

```
?importDatabase
```

# Documentation

PDF versions of the documentation are also available:
- Vignette: [Integration and aggregation of raw data into the OMOP CDM](https://gitlab.com/mathilde.frchrt/aggregate/-/tree/master/extras/aggregateStep.pdf)