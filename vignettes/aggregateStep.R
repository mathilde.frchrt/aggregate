## ---- echo = FALSE, message = FALSE, warning = FALSE--------------------------
#install.packages("drat")
#drat::addRepo("mathilde.frchrt/aggregate")
#install.packages("aggregate")

## ---- eval = FALSE------------------------------------------------------------
#  install.packages("drat")
#  drat::addRepo("mathilde.frchrt/aggregate")
#  install.packages("aggregate")

## ----eval=FALSE---------------------------------------------------------------
#  connectionDetails <- createConnectionDetails(dbms = "postgresql",
#                                               server = "localhost/ohdsi",
#                                               user = "joe",
#                                               password = "supersecret",
#                                               schema = "cdm")

## ----eval=FALSE---------------------------------------------------------------
#  listParameter <-  list(list(periodConceptId = 4,
#                                    method= totalPeriod,
#                                    measurementConceptId = 1,
#                                    subjectId = "person_id",
#                                    parameters = list(threshold = 0.3,
#                                                      operator = "<",
#                                                      timeUnit = "days",
#                                                      interpolationMethod = "sample-and-hold",
#                                                      delta= 5)),
#                              list(periodConceptId = 4,
#                                   method= areaBetweenCurveThreshold,
#                                   measurementConceptId = 1,
#                                   subjectId = "person_id",
#                              parameters = list(threshold = 0.3,
#                                                operator = "<")))

## ----eval=FALSE---------------------------------------------------------------
#  listParameter <-  list(list(periodConceptId = 4,
#                                    method= detectEpisode,
#                                    measurementConceptId = 1,
#                                    subjectId = "person_id",
#                                    parameters = list(threshold = 0.3,
#                                                      operator = "<",
#                                                      timeUnit = "days",
#                                                      interpolationMethod = "sample-and-hold",
#                                                      delta= 5)))

## ----eval=FALSE---------------------------------------------------------------
#  listParameter <-  list(list(periodConceptId = 4,
#                                    method= totalPeriod,
#                                    measurementConceptId = 1,
#                                    subjectId = "person_id",
#                                    parameters = list(threshold = 0.3,
#                                                      operator = "<",
#                                                      timeUnit = "days",
#                                                      interpolationMethod = "sample-and-hold",
#                                                      delta= 5)))

## ----eval=FALSE---------------------------------------------------------------
#  listParameter <-  list(list(periodConceptId = 4,
#                                    method= countEpisode,
#                                    measurementConceptId = 1,
#                                    subjectId = "person_id",
#                                    parameters = list(threshold = 0.3,
#                                                      operator = "<",
#                                                      timeUnit = "days",
#                                                      interpolationMethod = "sample-and-hold",
#                                                      delta= 5)))

## ----eval=FALSE---------------------------------------------------------------
#  listParameter <-  list(list(periodConceptId = 4,
#                                    method= maxWithoutNoise,
#                                    measurementConceptId = 1,
#                                    subjectId = "person_id",
#                                    parameters = list(maxValue = 120)),
#                        list(periodConceptId = 4,
#                                    method= minWithoutNoise,
#                                    measurementConceptId = 1,
#                                    subjectId = "person_id",
#                                    parameters = list(maxValue = 30)))

## ----eval=FALSE---------------------------------------------------------------
#  listParameter <-  list(list(periodConceptId = 4,
#                                    method= meanValue,
#                                    measurementConceptId = 1,
#                                    subjectId = "person_id",
#                                    parameters = list(min = 30, max = 120)))

## ----eval=FALSE---------------------------------------------------------------
#  listParameter <-  list(list(periodConceptId = 4,
#                                   method= areaBetweenCurveThreshold,
#                                   measurementConceptId = 1,
#                                   subjectId = "person_id",
#                                   parameters = list(threshold = 0.3,
#                                                    operator = "<")))

## ----eval=FALSE---------------------------------------------------------------
#  result = periodAggregatePivot(connectionDetails, pivotId = person_id, schemaName = "cdm_biology")

