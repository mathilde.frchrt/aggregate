CREATE SEQUENCE seq START 1 ; 

INSERT INTO period (period_id, person_id, period_concept_id, periode_start_datetime, 
periode_end_datetime, visit_occurrence_id, visit_detail_id)

SELECT nextval('seq'), 
	person_id,
	(SELECT min(concept_id) 
		FROM concept 
		WHERE concept_name = 'Psychiatry'
		AND vocabulary_id = 'Lille vocabulary'),	
	visit_start_datetime,
	visit_start_datetime + INTERVAL '28 day',
	visit_occurrence_id,
	visit_detail_id
	  
	FROM @cdm_database_schema.visit_occurrence

	INNER JOIN visit_detail
	ON visit_detail.visit_occurrence_id = visit_occurrence.visit_occurrence_id
	  
	WHERE visit_occurrence_concept_id = 9202 --- Outpatient
	AND provider_id = 1 --- Psychiatrist
	;