CREATE SEQUENCE seq START 1 ; 

INSERT INTO period (period_id, person_id, period_concept_id, periode_start_datetime, 
periode_end_datetime, visit_occurrence_id, visit_detail_id)

SELECT nextval('seq'), 
	person_id,
	(SELECT min(concept_id) 
		FROM concept 
		WHERE concept_name = 'Procedure'
		AND vocabulary_id = 'Lille vocabulary'),
	start.start_date,
	end_date,
	visit_occurrence_id,
	visit_detail_id
from
(SELECT person_id, visit_occurrence_id, visit_detail_id, 
	min(procedure_datetime) AS start_date
	FROM procedure_occurrence
	WHERE procedure_concept_id = 4082850    --- Induction
	GROUP BY person_id, visit_occurrence_id, visit_detail_id) AS start,

(SELECT person_id, visit_occurrence_id, visit_detail_id, 
	max(procedure_datetime) AS end_date
	FROM procedure_occurrence
	WHERE procedure_concept_id = 4160029    --- End of anesthesia
	GROUP BY person_id, visit_occurrence_id, visit_detail_id) AS 'end'
;

