DROP SEQUENCE IF EXISTS seq ; 

CREATE SEQUENCE seq START 1 ; 

INSERT INTO period (period_id, person_id, period_concept_id, periode_start_datetime, 
periode_end_datetime, visit_occurrence_id, visit_detail_id)

SELECT nextval('seq'),
	person_id,	
	 (SELECT min(concept_id) 
		FROM concept 
		WHERE concept_name = 'Séjour hospitalier'
		AND vocabulary_id = 'Lille vocabulary'),
	visit_start_datetime,
	visit_end_datetime,
	visit_occurrence_id,
	visit_detail_id

FROM @cdm_database_schema.visit_occurrence

INNER JOIN visit_detail
ON visit_detail.visit_occurrence_id = visit_occurrence.visit_occurrence_id
	   
WHERE visit_concept_id = 9201 --- Inpatient
OR visit_concept_id = 262 --- Emergency room & Inpatient
;