
CREATE SEQUENCE seq START 1 ; 

INSERT INTO period (period_id, person_id, period_concept_id, periode_start_datetime, 
periode_end_datetime, visit_occurrence_id, visit_detail_id)


SELECT nextval('seq'), 
	person_id AS subject_id,
	(SELECT min(concept_id) 
		FROM concept 
		WHERE concept_name = 'Anesthésie'
		AND vocabulary_id = 'Lille vocabulary'),
	start_date,
	end.end_date,
	visit_occurrence_id,
	visit_detail_id

(SELECT person_id,
	visit_occurrence_id,
	visit_detail_id,
	min(procedure_datetime) AS start_date,
	"procedure"
FROM procedure_occurrence
WHERE procedure_id = 4082850    --- induction
GROUP BY person_id, visit_occurrence_id, visit_detail_id, "procedure")

UNION ALL

(SELECT person_id,
	visit_occurrence_id,
	visit_detail_id,
	min(drug_exposure_start_date) AS start_date,
	"drug"
FROM drug_exposure
WHERE drug_exposure_id = 753626     --- propofol
GROUP BY person_id, visit_occurrence_id, visit_detail_id, "drug")

(SELECT person_id, visit_occurrence_id, visit_detail_id, 
	max(procedure_datetime) AS end_date
	FROM procedure_occurrence
	WHERE procedure_concept_id = 4160029    --- end of anesthesia
	GROUP BY person_id, visit_occurrence_id, visit_detail_id) AS end
	;

