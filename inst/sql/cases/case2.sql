CREATE SEQUENCE seq START 1 ; 

INSERT INTO period (period_id, person_id, period_concept_id, periode_start_datetime, 
periode_end_datetime, visit_occurrence_id, visit_detail_id)


SELECT nextval('seq'),
	person_id,
	(SELECT min(concept_id) 
		FROM concept 
		WHERE concept_name = 'Soins intensifs'
		AND vocabulary_id = 'Lille vocabulary'),
	visit_detail_start_datetime,
	visit_detail_end_datetime,
	visit_occurrence_id,
	visit_detail_id
	  
	FROM @cdm_database_schema.visit_detail
	  
	WHERE visit_detail_concept_id = 32037 --- Intensive Care
	;