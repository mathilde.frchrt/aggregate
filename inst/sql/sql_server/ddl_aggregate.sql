/************************

postgresql script to create OMOP aggregate tables

last revised: 27-Aug-2018

Authors:  Antoine Lamer, Mathilde Fruchart, Nicolas Paris, Adrien Parrot, Niels Martignène


*************************/

CREATE TABLE period (
  period_id							INTEGER			NOT NULL ,
  person_id							INTEGER			NOT NULL ,
  period_concept_id					INTEGER			NOT NULL ,
  start_period_event_id				INTEGER			NULL ,
  start_event_field_concept_id		INTEGER			NULL ,
  periode_start_date				DATE			NULL ,
  periode_start_datetime			TIMESTAMP		NULL ,
  end_period_event_id				INTEGER			NULL ,
  end_event_field_concept_id		INTEGER			NULL ,
  periode_end_date					DATE			NULL ,
  periode_end_datetime				TIMESTAMP		NULL ,	
  duration							INTEGER			NULL ,
  duration_unit_concept_id			INTEGER			NULL ,
  visit_occurrence_id				INTEGER			NULL ,
  visit_detail_id					INTEGER         NULL ,
  period_type_concept_id			INTEGER			NULL
);

CREATE TABLE period_aggregate (
  period_aggregate_id					INTEGER			NOT NULL ,
  person_id								INTEGER			NOT NULL ,
  period_id								INTEGER			NOT NULL ,
  period_concept_id						INTEGER			NULL ,
  raw_data_concept_id					INTEGER			NULL ,
  aggregation_method_concept_id			INTEGER			NULL ,
  value_as_number						NUMERIC			NULL ,
  value_as_concept_id					INTEGER			NULL ,
  visit_occurrence_id					INTEGER			NULL ,
  visit_detail_id						INTEGER			NULL ,	
  duration								INTEGER			NULL ,
  duration_unit_concept_id				INTEGER			NULL ,
  visit_occurrence_id					INTEGER			NULL ,
  visit_detail_id						INTEGER         NULL ,
  aggregate_type_concept_id				INTEGER			NULL ,
);

-- Primary Key

ALTER TABLE period ADD CONSTRAINT xpk_period PRIMARY KEY ( period_id ) ;

ALTER TABLE period_aggregate ADD CONSTRAINT xpk_period_aggregate PRIMARY KEY (period_aggregate_id);

-- Index

CREATE INDEX idx_period_id  ON period  (period_id ASC);

CREATE INDEX idx_period_aggregate_id ON period_aggregate (period_aggregate_id ASC);

-- Foreign Key

ALTER TABLE period ADD CONSTRAINT fpk_period_person FOREIGN KEY (person_id)  REFERENCES person (person_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_concept FOREIGN KEY (period_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_start_event FOREIGN KEY (start_event_field_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_end_event FOREIGN KEY (end_event_field_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_duration_unit FOREIGN KEY (duration_unit_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_visit_occurrence FOREIGN KEY (visit_occurrence_id)  REFERENCES visit_occurrence (visit_occurrence_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_visit_detail FOREIGN KEY (visit_detail_id)  REFERENCES visit_detail (visit_detail_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_type_concept FOREIGN KEY (period_type_concept_id)  REFERENCES concept (concept_id);


ALTER TABLE period ADD CONSTRAINT fpk_aggregate_person FOREIGN KEY (person_id)  REFERENCES person (person_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_period FOREIGN KEY (period_id)  REFERENCES period (period_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_period_concept FOREIGN KEY (period_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_period_concept FOREIGN KEY (period_concept_id)  REFERENCES concept (concept_id); 

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_raw_data_concept FOREIGN KEY (raw_data_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_method_concept FOREIGN KEY (aggregation_method_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_value_concept FOREIGN KEY (value_as_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_duration_concept FOREIGN KEY (duration_unit_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_visit_occurrence FOREIGN KEY (visit_occurrence_id)  REFERENCES visit_occurrence (visit_occurrence_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_visit_detail FOREIGN KEY (visit_detail_id)  REFERENCES visit_detail (visit_detail_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_type_concept FOREIGN KEY (aggregate_type_concept_id)  REFERENCES concept (concept_id);
